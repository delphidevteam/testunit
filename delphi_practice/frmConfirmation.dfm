object formConfirmation: TformConfirmation
  Left = 0
  Top = 0
  Caption = 'formConfirmation'
  ClientHeight = 161
  ClientWidth = 375
  Color = clMedGray
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object Confirm: TButton
    Left = 72
    Top = 72
    Width = 75
    Height = 25
    Caption = 'Confirm'
    DragCursor = crHandPoint
    TabOrder = 0
  end
  object Cancel: TButton
    Left = 216
    Top = 72
    Width = 75
    Height = 25
    Cancel = True
    Caption = 'Cancel'
    DragCursor = crHandPoint
    TabOrder = 1
    OnClick = CancelClick
  end
end
