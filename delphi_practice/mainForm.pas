unit mainForm;

interface

uses
  Winapi.Windows,
  Winapi.Messages,
  System.SysUtils,
  System.Variants,
  System.Classes,
  Vcl.Graphics,
  System.IOUtils,
  System.Generics.Collections,
  Vcl.Controls,
  Vcl.Forms,
  Vcl.Dialogs,
  Vcl.StdCtrls,
  Vcl.ExtCtrls;

const
  Zeljko_path = 'C:\Users\�eljko\Desktop\PersonData.txt';
  Tomislav_path = 'C:\Users\Tomislav\Desktop\PersonData.txt';

type
  TPersonRec = record

  public
    Name: string;
    Surname: string;
    AsString: string;
    function AsFriendlyString: string;

  public
    class function Create(Person: string): TPersonRec; overload; static;
    class function Create(Strings: TArray<string>): TPersonRec; overload; static;
    class function Empty: TPersonRec; static;
  end;

  TPersonList = class(TList<TPersonRec>)
  public
    procedure Add(Strings: TArray<string>); overload; // procedura Add vec postoji unutar TList<> sa drugim parametrom. Overload znaci da se procedura moze zvati isto Add ali mora primiti drugi parametar
    procedure Enumerate(const F: TFunc<TPersonRec, boolean>);
    procedure WriteToFile(const Path: string);
    procedure FromFile(const Path: string);
  end;

  TfrmMain = class(TForm)
    labelIntroductionApp: TLabel;
    nameField: TEdit;
    surnameField: TEdit;
    labelIme: TLabel;
    labelPrezime: TLabel;
    addButton: TButton;
    Panel1: TPanel;
    resultPanel: TMemo;
    closeButton: TButton;
    exportButton: TButton;
    panelList: TPanel;
    panelContent: TPanel;
    lbItems: TListBox;
    Label1: TLabel;
    Label2: TLabel;
    procedure closeButtonClick(Sender: TObject);
    procedure addButtonClick(Sender: TObject);
    procedure resultPanelStartDrag(Sender: TObject; var DragObject: TDragObject);
    procedure nameFieldChange(Sender: TObject);
    procedure surnameFieldChange(Sender: TObject);
    procedure exportButtonClick(Sender: TObject);
  private
    FPersonList: TPersonList;
    procedure DoCheck;
    procedure DoLoad;
  public
    destructor Destroy; override;
    procedure AfterConstruction; override;
  end;

var
  frmMain: TfrmMain;

implementation

{$R *.dfm}

procedure TfrmMain.AfterConstruction;
begin
  inherited;
  resultPanel.Text := string.Empty;
  FPersonList := TPersonList.Create;
  if Assigned(FPersonList) then
    DoLoad;
end;

procedure TfrmMain.DoLoad;
begin
  FPersonList.FromFile(Zeljko_path);
  FPersonList.Enumerate(
    function(APerson: TPersonRec): boolean
    begin
      Result := true;
      lbItems.AddItem(APerson.AsFriendlyString, nil);
      // drugi parametar je TObject - mozes tagirati neku klasu ili interface uz osobu. Ovdje nema taga pa je zato nil parametar proslijeden
    end);
end;

destructor TfrmMain.Destroy;
begin
  FPersonList.Free;
  inherited;
end;

procedure TfrmMain.DoCheck;
begin
  if (Length(nameField.Text) > 0) and (Length(surnameField.Text) > 0) then
    addButton.Enabled := true
  else
    addButton.Enabled := False;
end;

procedure TfrmMain.exportButtonClick(Sender: TObject);
begin
  FPersonList.WriteToFile(Zeljko_path);
end;

procedure TfrmMain.nameFieldChange(Sender: TObject);
begin
  DoCheck;
end;

procedure TfrmMain.surnameFieldChange(Sender: TObject);
begin
  DoCheck;
end;

procedure TfrmMain.addButtonClick(Sender: TObject);
begin
  case MessageDlg('Are you sure you want to add the member?', mtConfirmation, [mbYes, mbCancel], 0) of
    mrYes:
      begin
        resultPanel.Text := 'You have added ' + nameField.Text + ' ' + surnameField.Text;
        FPersonList.Add([nameField.Text, surnameField.Text]);
        lbItems.AddItem(FPersonList.Last.AsFriendlyString, nil);
      end;
    mrCancel:
      begin
        ShowMessage('Your member is not added!');
      end;
  end;
end;

procedure TfrmMain.closeButtonClick(Sender: TObject);
begin
  frmMain.Close;
end;

procedure TfrmMain.resultPanelStartDrag(Sender: TObject; var DragObject: TDragObject);
begin
  resultPanel.Text := '';
end;

{ TPersonRec }

class function TPersonRec.Create(Person: string): TPersonRec;
var
  LArray: TArray<string>;
begin
  Result := Empty;
  if Person.IsEmpty then
    Exit;

  Result.AsString := Person;

  LArray := Person.Split([',']);
  Result.Name := LArray[0];
  Result.Surname := LArray[1];
end;

class function TPersonRec.Create(Strings: TArray<string>): TPersonRec;
var
  I: Integer;
  LFullStr: string;
begin
  LFullStr := string.Empty;

  for I := Low(Strings) to High(Strings) do
  begin
    LFullStr := LFullStr + Strings[I];
    if not(I = High(Strings)) then
      LFullStr := LFullStr + ','
  end;

  Result := TPersonRec.Create(LFullStr);
end;

class function TPersonRec.Empty: TPersonRec;
begin
  FillChar(Result, SizeOf(Result), 0);
end;

function TPersonRec.AsFriendlyString: string;
begin
  Result := Name + ' ' + Surname;
end;

{ TPersonList }

procedure TPersonList.Enumerate(const F: TFunc<TPersonRec, boolean>);
var
  I: Integer;
begin
  for I := 0 to pred(Count) do
    if not F(Items[I]) then
      break;
end;

procedure TPersonList.Add(Strings: TArray<string>);
begin
  Add(TPersonRec.Create(Strings));
end;

procedure TPersonList.WriteToFile(const Path: string);
var
  LFile: string;
begin
  try
    LFile := string.Empty;
    Enumerate(
      function(APerson: TPersonRec): boolean
      begin
        Result := true;
        LFile := LFile + APerson.AsString + ';';
      end);

    TFile.WriteAllText(Path, LFile);
  finally
    ShowMessage('Objects successfully exported');
  end;
end;

procedure TPersonList.FromFile(const Path: string);
var
  LArray: TArray<string>;
  I: Integer;
begin
  if not TFile.Exists(Path) then
    Exit;

  LArray := TFile.ReadAllText(Path).Split([';']);
  for I := Low(LArray) to High(LArray) do
    if not LArray[I].IsEmpty then
      Add(TPersonRec.Create(LArray[I]));
end;

end.
